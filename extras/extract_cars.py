from bs4 import BeautifulSoup
import urllib.request as request
from extras.db_inter import get_cars_list, insert_car_names


root = 'https://www.carwale.com/'
path = root + '/new/'

html = request.urlopen(path)
soup = BeautifulSoup(html, 'html.parser')
soup_brands = soup.find('div', attrs={'class': 'brand-type-container'})

new_cars_list = []


def get_new_cars(is_first_time=False):
    for brand_span in soup_brands.find_all('a'):
        print('Processing... ' + brand_span.attrs['href'])
        html_tmp = request.urlopen(root + brand_span.attrs['href']).read().decode('utf-8')
        soup_tmp = BeautifulSoup(html_tmp, 'html.parser')
        soup_tmp_models = (soup_tmp.find('div', attrs={'id':'divModels'})
                       .find_all('div', attrs={'class':'list-seperator'}))

        for model_span in soup_tmp_models:
            model_span = model_span.find('a', attrs={'class': 'font18'})
            sub_model_html = request.urlopen(root+model_span.attrs['href']+'#versions')
            sub_model_soup = BeautifulSoup(sub_model_html, 'html.parser')

            for sub_model in (sub_model_soup.find('table', attrs={'id':'tblVersions'}).
                              find('tbody').find_all('tr')):
                new_cars_list.append(model_span.text + ' ' + sub_model.
                                     find('td', attrs={'valign':'top'}).find('a').text)

        if is_first_time:
            soup_tmp_dis_models = (soup_tmp.find('div', attrs={'id': 'discontinuedModels'}).
                                   find_all('a', attrs={'class': 'f-small'}))
            for dis_model in soup_tmp_dis_models:
                new_cars_list.append(dis_model.text)

    old_cars_list = get_cars_list()
    cars_difference = list(set(new_cars_list).difference(set(old_cars_list)))
    if cars_difference:
        print(cars_difference)
        insert_car_names(cars_difference, start=len(old_cars_list))  # there is a possibility of error here
    else:
        print('No new cars found!')


get_new_cars(is_first_time=True)