import math
import re

import pandas as pd
from sqlalchemy import MetaData
from extras.config import REG_NO_REGEX
from extras.config import classic_engine
from extras.db_inter import get_table, get_table_data, car_fields

path_cust = '/home/student/PycharmProjects/classic/RESOURCES/Classic Customers/Classic Records.xlsx'


def get_dict_list_xlsx(path):
    # extract xlsx file and convert to list of dicts
    return [x for x in
            pd.read_excel(path).
            replace(math.nan, '', regex=True).
            to_dict(orient='index').values()]


def convert_xls_to_db_old(path=path_cust, table_name='customer_vehicle', engine=classic_engine):
    xls_rows = get_dict_list_xlsx(path)
    meta = MetaData()
    car = get_table('car', engine=classic_engine)
    db_tuples = get_table_data('car', engine=classic_engine)
    model_names = {tuple[car_fields.model.value]: tuple[car_fields.car_id.value]
                   for tuple in db_tuples}
    regex = REG_NO_REGEX
    input('x')
    vehicle_insert_list = []
    customer_vehicle_insert_list = []
    customer_insert_list = []
    mob_no_list = []
    for row in xls_rows:
        flag = 0
        matched = regex.match(row['Reg No'])

        tmp_vehicle_dict = {}
        tmp_vehicle_dict['reg_no'] = ''.join(matched.groups()) if matched else None
        tmp_vehicle_dict['car_id'] = (model_names[row['Model']]
                                      if row['Model'] in model_names else None)
        tmp_vehicle_dict['km'] = int(row['KM']) if row['KM'] else None

        cust_mob = row['Mobile No']
        cust_reg = tmp_vehicle_dict['reg_no']

        if cust_reg and tmp_vehicle_dict['car_id']:
            flag += 1
            vehicle_insert_list.append(tmp_vehicle_dict)

        if cust_mob and cust_mob not in mob_no_list:
            flag += 2
            mob_no_list.append(cust_mob)
            customer_insert_list.append({'mob_no': int(row['Mobile No']),
                                         'name': row['Customer Name'],
                                         'address': None})
        if flag == 3 or (flag == 1 and cust_mob in mob_no_list):
            customer_vehicle_insert_list.append({'mob_no': cust_mob,
                                                 'reg_no': tmp_vehicle_dict['reg_no']})
        else:
            # Logging for uninserted row
            pass

    engine.connect().execute(get_table('customer').insert(),
                             customer_insert_list)
    engine.connect().execute(get_table('vehicle').insert(),
                             vehicle_insert_list)
    engine.connect().execute(get_table('customer_vehicle').insert(),
                             customer_vehicle_insert_list)


def convert_xls_to_db1(path=path_cust, table_name='customer_vehicle', engine=classic_engine):
    xls_rows = get_dict_list_xlsx(path)
    meta = MetaData()
    car = get_table('car', engine=classic_engine)
    db_tuples = get_table_data('car', engine=classic_engine)
    model_names = {tuple[car_fields.model.value]: tuple[car_fields.car_id.value]
                   for tuple in db_tuples}
    regex = REG_NO_REGEX
    input('x')
    vehicle_insert_list = []
    customer_vehicle_insert_list = []
    customer_insert_list = []
    mob_no_list = []
    for row in xls_rows:
        matched = regex.match(row['Reg No'])

        tmp_vehicle_dict = {}
        tmp_vehicle_dict['reg_no'] = ''.join(matched.groups()) if matched else None
        tmp_vehicle_dict['car_id'] = (model_names[row['Model']]
                                      if row['Model'] in model_names else None)
        tmp_vehicle_dict['km'] = int(row['KM']) if row['KM'] else None

        cust_mob = row['Mobile No']
        cust_reg = tmp_vehicle_dict['reg_no']

        if cust_mob and cust_mob not in mob_no_list:
            mob_no_list.append(cust_mob)
            customer_insert_list.append({'mob_no': int(row['Mobile No']),
                                         'name': row['Customer Name'],
                                         'address': None})
        if cust_reg and tmp_vehicle_dict['car_id'] and cust_mob in mob_no_list:
            tmp_vehicle_dict['mob_no'] = cust_mob
            vehicle_insert_list.append(tmp_vehicle_dict)
        else:
            # Logging for uninserted row
            pass

    engine.connect().execute(get_table('customer').insert(),
                             customer_insert_list)
    engine.connect().execute(get_table('vehicle').insert(),
                             vehicle_insert_list)

if __name__ == '__main__':
    convert_xls_to_db1(path_cust)

