from sqlalchemy import MetaData
from sqlalchemy.sql import select
from extras.config import classic_engine
from extras.config import (car_fields, customer_fields, vehicle_fields,
                           bill_fields)


class Vehicle():
    def __init__(self, reg_no, model, km, mob_no):
        self.reg_no = reg_no
        self.model = model
        self.km = km
        self.mob_no = mob_no


class Customer:
    def __init__(self, mob_no, name, address):
        self.mob_no = int(mob_no)
        self.name = name
        self.address = address


def get_customer_list(table_name=customer_fields.table_name.value, engine=classic_engine):
    return {row[customer_fields.mobile.value]:
                              Customer(row[customer_fields.mobile.value],
                                       row[customer_fields.name.value],
                                       row[customer_fields.address.value])
                          for row in
                              get_table_data(table_name, engine=engine)}


def get_customer_number_list(table_name=customer_fields.table_name.value, engine=classic_engine):
    return [key for key in sorted(get_customer_list(table_name, engine=engine).keys())]


def get_table(table_name, engine=classic_engine):
    meta = MetaData()
    meta.reflect(engine)
    return meta.tables[table_name]


def get_table_data(table_name, engine=classic_engine):
    table = get_table(table_name)
    return engine.connect().execute(select([table]))

def get_cars_list(engine=classic_engine, table_name=car_fields.table_name.value):
    table = get_table(table_name=table_name, engine=engine)
    cars = get_table_data(table_name, engine=engine)
    return {row[car_fields.model.value]: row[car_fields.car_id.value]
            for row in cars}

def get_vehicle_registration_nos(engine=classic_engine,
                                 table_name=vehicle_fields.table_name.value):
    vehicles = get_table_data(vehicle_fields.table_name.value)
    return [vehicle[vehicle_fields.registration.value]
            for vehicle in vehicles]


def get_vehicle_list(engine=classic_engine):
    vehicleT = get_table(vehicle_fields.table_name.value)
    carT = get_table(car_fields.table_name.value)
    vehicles = engine.connect().execute(select([vehicleT, carT]).
                                        where(vehicleT.c.car_id == carT.c.car_id))

    return {vehicle[vehicle_fields.registration.value]:
                Vehicle(vehicle[vehicle_fields.registration.value],
                        vehicle[car_fields.model.value],
                        vehicle[vehicle_fields.km.value],
                        vehicle[vehicle_fields.mob_no.value])
            for vehicle in vehicles}


def insert_car_names(model_list,engine=classic_engine,
                     table_name=car_fields.table_name.value, start=0):
    table = get_table(table_name, engine=engine)
    start = len(get_cars_list()) if not start else start
    engine.execute(table.insert(), [{'car_id': i + start,
                                     'model': model_list[i]}
                                    for i in range(len(model_list))])


def insert_bill(bill_id, reg_no, mob_no, contents, engine=classic_engine):
    table = get_table(bill_fields.table_name.value)
    engine.connect().execute(table.insert(),
                             [{bill_fields.bill_id.value: bill_id,
                              bill_fields.registration.value: reg_no,
                              bill_fields.mobile.value: mob_no,
                              bill_fields.bill.value: contents}])



if __name__ == '__main__':
    print(get_vehicle_list())

