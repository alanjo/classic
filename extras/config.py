from sqlalchemy import create_engine
from enum import Enum
import re

CLASSIC_DB_PATH = 'mysql+cymysql://root:R0ot@127.0.0.1:3306/classic'
classic_engine = create_engine(CLASSIC_DB_PATH)
MAX_BILL_SIZE = 100
MAX_TABLE_HEIGHT = 500 #__init_table table_height argument
REG_NO_REGEX = re.compile(r'([A-Z]{2,3})[ ]*([0-9]{0,2})[ ]*([A-Z]{0,2})[ ]*([0-9]{1,4})$')

SR_NO_WIDTH = 4
PARTICULARS_WIDTH = 35
PARTICULARS_REDUCED_WIDTH = PARTICULARS_WIDTH - 1
QUANTITY_WIDTH = 5
LABOUR_WIDTH = 8
MATERIAL_WIDTH = 8
TOTAL_WIDTH = (SR_NO_WIDTH + PARTICULARS_WIDTH + MATERIAL_WIDTH +
               QUANTITY_WIDTH + LABOUR_WIDTH + 5)

# TODO:Convert to int function for converting to int and returning 0 when None.
# TODO(Done): UPdate the customer details into the database if changed(make dialog box for confirmation), insert new customer
# TODO(DOne):If customer is not in database, it should be automatically added.
# TODO(DOne):VALIDATION on the input boxes[Mob No and Reg No especially]
# TODO(Done): ADD the cars database and also the customer cars database
# TODO(Done, made a qdialog):Find a way to deactivate the bill_desk after submit has been called [deactivate submit, or deactivate Qtable]
# TODO(Done):Convert The customer records xlsx file to classic.customer db
# TODO(Done): No need for search by the way auto completer handles it):Implement the search function on mob No(customer) and vehicle no.(vehicle)[use substring]
# TODO(Done):A dialog box before bill is submitted.
# TODO(Done):find some way to order the input boxes in billdesk


class customer_fields(Enum):
    table_name = 'customer'
    name = 'name'
    mobile = 'mob_no'
    address = 'address'

class vehicle_fields(Enum):
    table_name = 'vehicle'
    registration = 'reg_no'
    car_id = 'car_id'
    km = 'km'
    mob_no = 'mob_no'

class car_fields(Enum):
    table_name = 'car'
    car_id = 'car_id'
    model = 'model'

class bill_fields(Enum):
    table_name = 'bill'
    bill_id = 'bill_id'
    registration = 'reg_no'
    mobile = 'mob_no'
    bill = 'contents'
