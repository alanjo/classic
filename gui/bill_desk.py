import json
import sys
from datetime import date, datetime
from PyQt5.QtWidgets import (QWidget, QPushButton, QLabel, QHBoxLayout,
                             QTableWidget, QHeaderView, QVBoxLayout, QLineEdit,
                             QApplication, QDialog, QCalendarWidget, QTextEdit,
                             QTableWidgetItem)
from PyQt5.Qt import QPrintDialog, QPrinter, QColor
from extras.config import (MAX_BILL_SIZE, MAX_TABLE_HEIGHT, classic_engine, REG_NO_REGEX,
                           customer_fields, bill_fields, vehicle_fields)
from extras.config import (SR_NO_WIDTH, PARTICULARS_WIDTH, PARTICULARS_REDUCED_WIDTH,
                           QUANTITY_WIDTH, MATERIAL_WIDTH, LABOUR_WIDTH, TOTAL_WIDTH)
from extras.db_inter import (get_vehicle_registration_nos, get_vehicle_list,
                             get_cars_list, get_customer_list, get_customer_number_list, get_table,
                             insert_bill, insert_car_names)
from sqlalchemy.exc import IntegrityError
from gui.ui_extras import DropDownTextEdit
from itertools import compress
#for testing
import random
from string import ascii_letters


class MyTable(QTableWidget):
    def __init__(self, col_names, r, height):
        c = len(col_names)
        super().__init__(r, c)
        self.col_names = col_names
        self.no_of_rows = r
        self.entered_list = [{} for x in range(r)]
        self.setHorizontalHeaderLabels(col_names)
        self.setFixedHeight(height)
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.setSortingEnabled(False)
        self.cellChanged.connect(self.on_changed)

    def on_changed(self):
        r = self.currentRow()
        c = self.currentColumn()
        key = self.col_names[c]
        self.entered_list[r][key] = self.item(r, c).text()
        #self.print_entered_list()

    def clearContents(self):
        super().clearContents()
        self.entered_list = [{} for x in range(self.no_of_rows)]

    def cleaned_list(self):
        return [r for r in self.entered_list if r]

    def print_entered_list(self):
        cleaned = self.cleaned_list()
        [print(r, cleaned[r]) for r in range(len(cleaned))]

    def run_random(self):
        rand_rows = random.randint(1, self.no_of_rows)
        combo_domain = ' ' + ascii_letters

        self.clearContents()
        for i in range(rand_rows):
            x = random.randint(10, 50)
            self.setCurrentCell(i, 0)
            self.setItem(i, 0, QTableWidgetItem(''.join(random.sample(combo_domain,x))))
            self.setCurrentCell(i, 1)
            self.setItem(i, 1, QTableWidgetItem(str(random.randint(10, 100))))
            self.setCurrentCell(i, 2)
            self.setItem(i, 2, QTableWidgetItem(str(random.randint(100, 5000))))
            self.setCurrentCell(i, 3)
            self.setItem(i,3, QTableWidgetItem(str(random.randint(100, 5000))))
        #print(self.get_as_string())
        #self.setSortingEnabled(True)

    def get_as_string(self):
        cleaned = self.cleaned_list()


        in_between_header_fill = ' '
        in_between_content_fill = ' '
        header_fill = '|'
        content_fill = '|'
        horizontal_border = '_' * (PARTICULARS_WIDTH + QUANTITY_WIDTH + LABOUR_WIDTH +
                                   MATERIAL_WIDTH + SR_NO_WIDTH + 6) + '\n'

        formatted_string = horizontal_border
        formatted_string += ('{fill}{0:{ifill}^{sr_w}}{fill}{1:{ifill}^{p_w}}{fill}'
                             '{2:{ifill}^{q_w}}{fill}'
                            '{3:{ifill}^{m_w}}{fill}{4:{ifill}^{l_w}}{fill}\n'.
                            format('Sr.', 'Particulars', 'Qty', 'Material', 'Labour',
                                   sr_w=SR_NO_WIDTH, p_w=PARTICULARS_WIDTH,
                                   q_w=QUANTITY_WIDTH, m_w=MATERIAL_WIDTH,
                                   l_w=LABOUR_WIDTH, fill=header_fill, ifill=in_between_header_fill))
        formatted_string += horizontal_border
        for index, row in enumerate(cleaned):
            no_of_particulars_slices = len(row['Particulars']) // PARTICULARS_REDUCED_WIDTH
            particulars_sliced = [row['Particulars'][i * PARTICULARS_REDUCED_WIDTH:(i + 1) * PARTICULARS_REDUCED_WIDTH]
                                  for i in range(no_of_particulars_slices + 1)]

            formatted_string += ('{fill}{0:{ifill}^{sr_w}}{fill}{1:{ifill}<{p_w}}{fill}'
                                 '{2:{ifill}>{q_w}}{fill}'
                                 '{3:{ifill}>{m_w}}{fill}'
                                 '{4:{ifill}>{l_w}}{fill}\n'.
                                format(index + 1,particulars_sliced[0],
                                       row['Qty'], row['Material'], row['Labour'],
                                       sr_w=SR_NO_WIDTH, p_w=PARTICULARS_WIDTH,
                                       q_w=QUANTITY_WIDTH, m_w=MATERIAL_WIDTH,
                                       l_w=LABOUR_WIDTH, fill=content_fill, ifill=in_between_content_fill))

            for sliced_string in particulars_sliced[1:]:
                formatted_string += ('{fill}{0:^{sr_w}}{fill}{1:<{p_w}}{fill}{2:>{q_w}}'
                                     '{fill}{3:>{m_w}}{fill}'
                                     '{4:>{l_w}}{fill}\n'.
                                     format(' ', sliced_string,
                                            ' ', ' ', ' ',
                                            sr_w=SR_NO_WIDTH, p_w=PARTICULARS_WIDTH,
                                            q_w=QUANTITY_WIDTH, m_w=MATERIAL_WIDTH,
                                            l_w=LABOUR_WIDTH, fill=content_fill))

        formatted_string += horizontal_border

        return formatted_string


class BillDesk(QWidget):
    def __init__(self):
        super().__init__()
        v_box = QVBoxLayout()

        v_box1 = QVBoxLayout()
        h_box = QHBoxLayout()
        h_box.addWidget(QLabel('Mobile No.'))
        h_box.addStretch()
        no_list = [str(i) for i in get_customer_number_list()]
        self.mob_no_le = DropDownTextEdit(completer_list=no_list)
        h_box.addWidget(self.mob_no_le)
        v_box1.addLayout(h_box)

        h_box = QHBoxLayout()
        h_box.addWidget(QLabel('Name'))
        h_box.addStretch()
        self.name_le = QLineEdit()
        h_box.addWidget(self.name_le)
        v_box1.addLayout(h_box)

        h_box = QHBoxLayout()
        h_box.addWidget(QLabel('Address'))
        h_box.addStretch()
        self.add_le = QLineEdit()
        h_box.addWidget(self.add_le)
        v_box1.addLayout(h_box)

        v_box2 = QVBoxLayout()
        h_box = QHBoxLayout()
        h_box.addWidget(QLabel('Registration'))
        h_box.addStretch()
        self.reg_le = DropDownTextEdit(completer_list=get_vehicle_registration_nos())
        h_box.addWidget(self.reg_le)
        v_box2.addLayout(h_box)

        h_box = QHBoxLayout()
        h_box.addWidget(QLabel('KMs Run'))
        h_box.addStretch()
        self.km_le = QLineEdit()
        h_box.addWidget(self.km_le)
        v_box2.addLayout(h_box)

        h_box = QHBoxLayout()
        h_box.addWidget(QLabel('Model'))
        h_box.addStretch()
        self.model_le = DropDownTextEdit(completer_list=sorted(get_cars_list()))
        h_box.addWidget(self.model_le)
        v_box2.addLayout(h_box)

        h_box = QHBoxLayout()
        h_box.addWidget(QLabel('Date'))
        h_box.addStretch()
        self.date_le = QLineEdit()
        h_box.addWidget(self.date_le)
        self.cal_bt = QPushButton()
        self.__init__date_picker()
        h_box.addWidget(self.cal_bt)
        v_box2.addLayout(h_box)

        self.b_submit = QPushButton('RunRandom')
        self.b_clear = QPushButton('Clear All')
        self.b_bill_submit = QPushButton('Make Bill')

        self.table = MyTable(['Particulars', 'Qty', 'Material', 'Labour'], MAX_BILL_SIZE,
                           MAX_TABLE_HEIGHT)
        self.b_submit.clicked.connect(self.run_random)

        h_box1 = QHBoxLayout()
        h_box1.addLayout(v_box1)
        h_box1.addLayout(v_box2)
        h_box1.addStretch()

        v_box.addLayout(h_box1)

        #v_box.addWidget(self.b_submit)
        v_box.addStretch()
        v_box.addWidget(self.table)
        v_box.addWidget(self.b_bill_submit)
        v_box.addWidget(self.b_clear)
        v_box.addWidget(self.b_submit)
        self.setLayout(v_box)

        self.__init__widget_attributes()
        #self.showMaximized()
        self.show()

    def __init__widget_attributes(self):
        self.b_bill_submit.clicked.connect(self.b_bill_submit_clicked)
        self.mob_no_le.completer.activated.connect(self.mob_no_completed)
        self.reg_le.completer.activated.connect(self.reg_completed)
        self.b_clear.clicked.connect(self.clear_widgets)

        today = date.today()
        self.date_le.setText(str(today.day)+'-'+str(today.month)+'-'+str(today.year))

    def __init__date_picker(self):
        self.cal = QCalendarWidget()
        self.cal.setGridVisible(True)

        self.cal_dialog = QDialog()
        h_box = QVBoxLayout()
        h_box.setContentsMargins(0, 0, 0, 0)
        h_box.addWidget(self.cal)
        self.cal_dialog.setWindowTitle('Choose Date')
        self.cal_dialog.setLayout(h_box)
        self.cal.clicked.connect(self.set_date)
        self.cal_bt.clicked.connect(lambda:self.cal_dialog.exec_())

    def run_random(self):
        self.table.run_random()
        vehicles = get_vehicle_list()
        customers = get_customer_list()

        vehicle = random.choice(list(vehicles.keys()))
        customer = str(random.choice(list(customers.keys())))

        self.reg_le.setText(vehicle)
        self.reg_completed(vehicle)
        self.mob_no_le.setText(customer)
        self.mob_no_completed(customer)
        self.date_le.setText('1-9-1999')

    def b_bill_submit_clicked(self):
        dialog = QDialog()
        v_box = QVBoxLayout()
        dialog.setWindowTitle('Bill Process')
        dialog.yes_button = QPushButton('Yes')
        dialog.no_button = QPushButton('No')
        dialog_label = QLabel('Submit Bill?')
        v_box.addWidget(dialog_label)
        h_box = QHBoxLayout()
        h_box.addWidget(dialog.yes_button)
        h_box.addWidget(dialog.no_button)
        dialog.yes_button.clicked.connect(dialog.accept)
        dialog.no_button.clicked.connect(dialog.reject)
        dialog.no_button.setDefault(True)
        v_box.addLayout(h_box)
        dialog.setLayout(v_box)
        if dialog.exec_():  # returns 0 or 1 depending on whether accept or reject
            v = self.validate()
            if not v:
                self.db_changes()
                self.print_preview()
            else:
                compressor = [int(i) for i in bin(v)[2:]]
                dialog_tmp = QDialog()
                dialog_tmp.setWindowTitle('ERROR!')
                l2 = QLabel('Check Registration')
                l = QLabel('Mobile No. should be a 10 digit number')
                l1 = QLabel('KMs Run should be a number')
                dialog_tmp.b = QPushButton('OK')
                dialog_tmp.b.clicked.connect(dialog_tmp.accept)
                v_box1 = QVBoxLayout()
                [v_box1.addWidget(x) for x in compress([l2, l, l1], compressor)]

                v_box1.addWidget(dialog_tmp.b)
                dialog_tmp.setLayout(v_box1)
                dialog_tmp.exec_()

    def print_preview(self):
        dialog = QDialog()
        dialog.setWindowTitle('Print Preview')
        v = QVBoxLayout()
        text = QTextEdit()
        but = QPushButton(text='Print')
        v.addWidget(text)
        v.addWidget(but)
        dialog.setLayout(v)
        text.setFixedSize(TOTAL_WIDTH * 10, 80 * 10)
        text.setFontFamily('DejaVu Sans Mono')
        text.setTextColor(QColor(0, 0, 255))
        text.setText('{0:{ifill}^{wid}}'.format('CLASSIC CAR CARE', ifill='-', wid = TOTAL_WIDTH))

        text.setTextColor(QColor(0,0,0))
        text.append(self.get_as_string())#93*49
        dialog.setModal(True)
        but.clicked.connect(lambda: self.print(text, dialog))
        dialog.exec_()

    def print(self, text, dialog):
        printer = QPrinter()
        print_dialog = QPrintDialog(printer)
        if print_dialog.exec_():
            text.print(printer)
            dialog.hide()

    def get_as_string(self):
        ifill = ' '
        fill = ' '
        wid1 = TOTAL_WIDTH // 2
        wid2 = wid1
        formatted_string_edits = ('{0:{ifill}<{wid1}}{fill}{1:{ifill}>{wid2}}{fill}\n' \
                                 '{2:{ifill}<{wid1}}{fill}{3:{ifill}>{wid2}}{fill}\n' \
                                 '{4:{ifill}<{wid1}}{fill}{5:{ifill}>{wid2}}{fill}\n' \
                                 '{6:{ifill}<{wid1}}{fill}{7:{ifill}>{wid2}}{fill}\n'.
                                  format(self.mob_no_le.text(), self.reg_le.text(),
                                         self.name_le.text(), self.model_le.text(),
                                         self.add_le.text(), self.km_le.text(),
                                         ' ', self.date_le.text(),
                                         ifill=ifill, fill=fill, wid1=wid1, wid2=wid2))
        return formatted_string_edits + self.table.get_as_string()

    def validate(self):
        mob = reg_no = km = None
        matched = REG_NO_REGEX.match(self.reg_le.text())
        v = 0
        if matched:
            reg_no = ''.join(matched.groups())
        else:
            v += 1

        if len(self.mob_no_le.text()) >= 10:
            try:
                mob = int(self.mob_no_le.text())
            except ValueError:
                # Logging for not an int
                v += 2
        else:
            v += 2

        try:
            km = int(self.km_le.text()) if self.km_le.text() else 0
        except ValueError:
            v += 4
            # Logging for not an int

        return v

    def db_changes(self):
        reg_no = self.reg_le.text()
        mob_no = int(self.mob_no_le.text())
        name = self.name_le.text()
        address = self.add_le.text() if self.add_le.text() else None
        km = int(self.km_le.text())
        model = self.model_le.text()

        mob_no_list = get_customer_number_list()
        reg_no_list = get_vehicle_registration_nos()
        cust_list = get_customer_list()
        vehicle_list = get_vehicle_list()
        cars_list = get_cars_list()

        cust_table = get_table(customer_fields.table_name.value)
        vehicle_table = get_table(vehicle_fields.table_name.value)
        bill_table = get_table(bill_fields.table_name.value)

        print('bla')
        if model not in cars_list:
            d = QDialog()
            v = QVBoxLayout()
            v.addWidget(QLabel('Add New Model?'))

            d.b_yes = QPushButton()
            d.b_yes.setText('Yes')
            d.b_no = QPushButton()
            d.b_no.setText('No')

            d.b_yes.clicked.connect(d.accept)
            d.b_no.clicked.connect(d.reject)
            h = QHBoxLayout()
            h.addWidget(d.b_yes)
            h.addWidget(d.b_no)

            v.addLayout(h)
            d.setLayout(v)

            if d.exec_():
                insert_car_names([model])
                cars_list = get_cars_list()
            else:
                print('Not Inserted')
                return 0

        if mob_no in mob_no_list:
            if name == cust_list[mob_no].name and address == cust_list[mob_no].address:
                pass
            else:
                # Logging for updated entry
                classic_engine.connect().execute(cust_table.
                                                 update().
                                                 where(cust_table.c.mob_no == mob_no).
                                                 values(name=name, address=address))
        else:
            # Logging for inserted entry
            classic_engine.connect().execute(cust_table.
                                             insert(),
                                             [{customer_fields.name.value: name,
                                               customer_fields.address.value: address,
                                               customer_fields.mobile.value: mob_no}])

        if reg_no in reg_no_list:
            if km == vehicle_list[reg_no].km and cars_list[model] == cars_list[vehicle_list[reg_no].model]:
                print('12')
                pass
            else:
                print('124')
                try:
                    classic_engine.connect().execute(vehicle_table.update().
                                                 where(vehicle_table.c.reg_no == reg_no).
                                                 values({vehicle_fields.car_id.value: cars_list[model],
                                                         vehicle_fields.km.value: km}))
                except IntegrityError:
                    # Logging
                    print('Dubious entry')
                    return 0

        else:
            classic_engine.connect().execute(vehicle_table.insert(),
                                             [{vehicle_fields.car_id.value: cars_list[model],
                                               vehicle_fields.registration.value:reg_no,
                                               vehicle_fields.km.value: km,
                                               vehicle_fields.mob_no.value: mob_no}])
        cleaned = self.table.cleaned_list()
        json_dump = json.dumps(cleaned)
        insert_bill(self.get_bill_id(), reg_no, mob_no,
                    json_dump, engine=classic_engine)
        print('blaaa')

    def get_bill_id(self):
        return int(datetime.now().strftime('%y%m%d%H%M%S'))

    def clear_widgets(self):
        self.name_le.clear()
        self.add_le.clear()
        self.mob_no_le.clear()
        self.reg_le.clear()
        self.km_le.clear()
        self.model_le.clear()
        self.date_le.clear()
        self.table.clearContents()

    def mob_no_completed(self, text):
        customer = get_customer_list()[int(text)]
        self.name_le.setText(customer.name)
        self.add_le.setText(customer.address)

    def reg_completed(self, text):
        vehicles = get_vehicle_list()
        self.model_le.setText(vehicles[text].model)
        self.km_le.setText(str(vehicles[text].km))

    def set_date(self, date):
        self.date_le.setText(str(date.day())+'-'+str(date.month())+'-'+str(date.year()))
        self.cal_dialog.done(1)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    c = BillDesk()
    sys.exit(app.exec_())
