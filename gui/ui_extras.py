from PyQt5.Qt import QCompleter, QTextCursor, QApplication
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtCore import Qt
import sys


class Completer(QCompleter):
    def __init__(self, completer_list=None, parent=None):
        QCompleter.__init__(self, completer_list, parent)
        self.setCompletionMode(QCompleter.PopupCompletion)
        self.highlighted.connect(self.on_highlight)
        self.last_selection = ''
        self.setCaseSensitivity(Qt.CaseInsensitive)

    def on_highlight(self, text):
        self.last_selection = text

    def get_last_selection(self):
        return self.last_selection


class DropDownTextEdit(QLineEdit):
    def __init__(self, completer_list=['212', '2114', '213'], *args, **kwargs):
        QLineEdit.__init__(self, *args, **kwargs)
        self.completer = Completer(completer_list=completer_list)
        self.completer.setWidget(self)
        self.setCompleter(self.completer)

    def setText(self, p_str):
        QLineEdit.setText(self, p_str)

    def keyPressEvent(self, key_event):
        QLineEdit.keyPressEvent(self, key_event)
        # print(key_event.text(), self.text(), self.completer.last_selection)
        self.completer.setCompletionPrefix(self.text())
        if key_event.key() == Qt.Key_Tab and self.completer.popup().isVisible():
            popup = self.completer.popup()
            popup.setCurrentIndex(self.completer.completionModel().index(0, 0))
            print(self.completer.get_last_selection())

if __name__ == '__main__':
    app = QApplication(sys.argv)
    c = DropDownTextEdit()
    c.show()
    sys.exit(app.exec_())
