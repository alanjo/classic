-- MySQL dump 10.13  Distrib 5.7.16, for Win64 (x86_64)
--
-- Host: localhost    Database: classic
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `emp`
--

DROP TABLE IF EXISTS `emp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emp` (
  `emp_id` char(10) NOT NULL,
  `emp_name` varchar(20) NOT NULL,
  `emp_lname` varchar(20) DEFAULT NULL,
  `join_date` datetime DEFAULT NULL,
  `mob_no` char(10) DEFAULT NULL,
  `job` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp`
--

LOCK TABLES `emp` WRITE;
/*!40000 ALTER TABLE `emp` DISABLE KEYS */;
INSERT INTO `emp` VALUES ('ALHR160512','Rahul',NULL,'2016-05-12 00:00:00','8943497362',NULL),('ANAJ160622','Jaswant',NULL,'2016-06-22 00:00:00','9567319720',NULL),('DLPI161205','Dilip',NULL,'2016-12-05 00:00:00','9142050902',NULL),('HPAL161205','Manichand','Puthupally','2016-12-05 00:00:00','9847849587',NULL),('IABM161205','Thambi',NULL,'2016-12-05 00:00:00','9847525616',NULL),('IMJR111128','Baiju','Kumar','2011-11-28 00:00:00','9946971098',NULL);
/*!40000 ALTER TABLE `emp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_exp`
--

DROP TABLE IF EXISTS `emp_exp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emp_exp` (
  `emp_id` char(10) NOT NULL,
  `date` datetime DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `reason` varchar(50) DEFAULT NULL,
  KEY `emp_id` (`emp_id`),
  CONSTRAINT `emp_exp_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_exp`
--

LOCK TABLES `emp_exp` WRITE;
/*!40000 ALTER TABLE `emp_exp` DISABLE KEYS */;
INSERT INTO `emp_exp` VALUES ('IMJR111128','2016-10-09 00:00:00',71468,'Advance Amount remaining '),('ALHR160512','2016-05-29 00:00:00',2000,'Advance'),('ALHR160512','2016-06-13 00:00:00',500,'Advance'),('ALHR160512','2016-06-21 00:00:00',500,'Advance'),('ALHR160512','2016-07-02 00:00:00',1000,'Advance'),('ALHR160512','2016-07-02 00:00:00',5057,'Advance'),('ALHR160512','2016-07-10 00:00:00',500,'Advance'),('ALHR160512','2016-07-19 00:00:00',1000,'Advance'),('ALHR160512','2016-07-28 00:00:00',500,'Advance'),('ALHR160512','2016-08-13 00:00:00',1000,'Advance'),('ALHR160512','2016-08-28 00:00:00',500,'Advance'),('ALHR160512','2016-08-30 00:00:00',100,'Advance'),('ALHR160512','2016-09-01 00:00:00',100,'Advance'),('ALHR160512','2016-09-02 00:00:00',500,'Advance'),('ALHR160512','2016-09-08 00:00:00',100,'Advance'),('ALHR160512','2016-09-10 00:00:00',3000,'Advance'),('ALHR160512','2016-09-20 00:00:00',100,'Advance'),('ALHR160512','2016-09-27 00:00:00',500,'Advance'),('ALHR160512','2016-10-02 00:00:00',100,'Advance'),('ANAJ160622','2016-06-21 00:00:00',100,'Advance Given for mobile Charging'),('ANAJ160622','2016-07-10 00:00:00',200,'Advance Given for mobile Charging'),('ANAJ160622','2016-07-23 00:00:00',300,'Advance Given for mobile Charging'),('ANAJ160622','2016-07-30 00:00:00',100,'Advance Given for mobile Charging'),('ANAJ160622','2016-08-12 00:00:00',200,'Advance Given for mobile Charging'),('ANAJ160622','2016-09-01 00:00:00',200,'Advance Given for mobile Charging'),('ANAJ160622','2016-09-25 00:00:00',200,'Advance Given for mobile Charging'),('ANAJ160622','2016-10-03 00:00:00',300,'Advance Given for mobile Charging');
/*!40000 ALTER TABLE `emp_exp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_leave`
--

DROP TABLE IF EXISTS `emp_leave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emp_leave` (
  `emp_id` char(10) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  PRIMARY KEY (`emp_id`,`start_date`,`end_date`),
  CONSTRAINT `emp_leave_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `emp` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_leave`
--

LOCK TABLES `emp_leave` WRITE;
/*!40000 ALTER TABLE `emp_leave` DISABLE KEYS */;
INSERT INTO `emp_leave` VALUES ('IMJR111128','2012-07-20 00:00:00','2012-08-06 00:00:00'),('IMJR111128','2013-03-28 00:00:00','2013-04-22 00:00:00'),('IMJR111128','2013-08-01 00:00:00','2013-09-04 00:00:00'),('IMJR111128','2013-12-31 00:00:00','2014-01-19 00:00:00'),('IMJR111128','2014-11-01 00:00:00','2014-11-26 00:00:00'),('IMJR111128','2015-05-01 00:00:00','2015-05-26 00:00:00'),('IMJR111128','2015-12-01 00:00:00','2015-12-06 00:00:00'),('IMJR111128','2016-01-13 00:00:00','2016-01-15 00:00:00'),('IMJR111128','2016-04-15 00:00:00','2016-04-19 00:00:00'),('IMJR111128','2016-05-16 00:00:00','2016-05-16 00:00:00'),('IMJR111128','2016-06-13 00:00:00','2016-06-13 00:00:00'),('IMJR111128','2016-07-11 00:00:00','2016-07-11 00:00:00'),('IMJR111128','2016-08-24 00:00:00','2016-08-28 00:00:00'),('IMJR111128','2016-09-13 00:00:00','2016-09-19 00:00:00');
/*!40000 ALTER TABLE `emp_leave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `emp_named_exp`
--

DROP TABLE IF EXISTS `emp_named_exp`;
/*!50001 DROP VIEW IF EXISTS `emp_named_exp`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `emp_named_exp` AS SELECT 
 1 AS `emp_id`,
 1 AS `emp_name`,
 1 AS `emp_lname`,
 1 AS `date`,
 1 AS `amount`,
 1 AS `reason`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `emp_named_leave`
--

DROP TABLE IF EXISTS `emp_named_leave`;
/*!50001 DROP VIEW IF EXISTS `emp_named_leave`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `emp_named_leave` AS SELECT 
 1 AS `emp_id`,
 1 AS `emp_name`,
 1 AS `emp_lname`,
 1 AS `mob_no`,
 1 AS `start_date`,
 1 AS `end_date`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `emp_named_exp`
--

/*!50001 DROP VIEW IF EXISTS `emp_named_exp`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `emp_named_exp` AS select `emp`.`emp_id` AS `emp_id`,`emp`.`emp_name` AS `emp_name`,`emp`.`emp_lname` AS `emp_lname`,`emp_exp`.`date` AS `date`,`emp_exp`.`amount` AS `amount`,`emp_exp`.`reason` AS `reason` from (`emp` join `emp_exp`) where (`emp`.`emp_id` = `emp_exp`.`emp_id`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `emp_named_leave`
--

/*!50001 DROP VIEW IF EXISTS `emp_named_leave`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `emp_named_leave` AS select `emp`.`emp_id` AS `emp_id`,`emp`.`emp_name` AS `emp_name`,`emp`.`emp_lname` AS `emp_lname`,`emp`.`mob_no` AS `mob_no`,`emp_leave`.`start_date` AS `start_date`,`emp_leave`.`end_date` AS `end_date` from (`emp` join `emp_leave`) where (`emp`.`emp_id` = `emp_leave`.`emp_id`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-08 13:53:05
